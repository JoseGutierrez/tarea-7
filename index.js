const express = require("express");
const app = express();
const port = 3000;

//rutas
const descripcionRouter = require("./routes/descripcion.router");
const sumaRouter = require("./routes/suma.router");
const restaRouter = require("./routes/resta.router");
const multiplicacionRouter = require("./routes/multiplicacion.router");
const divisionRouter = require("./routes/division.router");

const router = express.Router();

app.use(express.json());

app.use("/api", router);
//nombre de las rutas
router.use("/", descripcionRouter);
router.use("/operations/sum", sumaRouter);
router.use("/operations/subtraction", restaRouter);
router.use("/operations/multiplication", multiplicacionRouter);
router.use("/operations/division", divisionRouter);

app.listen(port, () => {
  console.log("http://localhost:3000/api");
  console.log("http://localhost:3000/api/operations/sum");
  console.log("http://localhost:3000/api/operations/subtraction");
  console.log("http://localhost:3000/api/operations/multiplication");
  console.log("http://localhost:3000/api/operations/division");
});
