class restaService {
  constructor() {}
  async resta(body) {
    const { number1, number2 } = body;
    return { result: number1 - number2 };
  }
}

module.exports = restaService;
