class divisionService {
  constructor() {}
  async division(body) {
    const { quotient, divisor } = body;
    return { result: quotient / divisor };
  }
}

module.exports = divisionService;
