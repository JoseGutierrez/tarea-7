class sumaService {
  constructor() {}
  async suma(body) {
    const { number1, number2 } = body;
    return { result: number1 + number2 };
  }
}

module.exports = sumaService;
