class multiplicacionService {
  constructor() {}
  async multiplicacion(body) {
    const { number1, number2 } = body;
    return { result: number1 * number2 };
  }
}

module.exports = multiplicacionService;
