const express = require("express");
const restaService = require("../service/resta.service");
const router = express.Router();
const service = new restaService();

router.get("/", async (req, res) => {
  const body = req.body;
  if (Object.entries(body).length === 0)
    return res.status(400).json({ msg: "El Body esta vacio" });

  if (!body.number1)
    return res.status(400).json({ msg: "Falta el PRIMER numero" });

  if (!body.number2)
    return res.status(400).json({ msg: "Falta el SEGUNDO NUMERO" });

  if (!Number.isInteger(body.number1))
    return res
      .status(400)
      .json({ msg: "El PRIMER numero no es del tipo numerico" });

  if (!Number.isInteger(body.number2))
    return res
      .status(400)
      .json({ msg: "El SEGUNDO numero no es del tipo numerico" });

  return res.status(200).json(await service.resta(body));
});

module.exports = router;
