const express = require("express");
const descripcionService = require("../service/descripcion.service");
const router = express.Router();
const service = new descripcionService();

router.get("/", async (req, res) => {
  res.status(200).json(await service.descripcion());
});

module.exports = router;
