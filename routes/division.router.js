const express = require("express");
const divisionService = require("../service/division.service");
const router = express.Router();
const service = new divisionService();

router.get("/", async (req, res) => {
  const body = req.body;

  if (Object.entries(body).length === 0)
    return res.status(400).json({ msg: "El Body esta vacio" });

  if (!body.quotient) return res.status(400).json({ msg: "Falta el COCIENTE" });

  if (body.divisor == 0)
    return res.status(400).json({ msg: "El DIVISOR debe ser distinto a 0" });

  if (!body.divisor) return res.status(400).json({ msg: "Falta el DIVISOR" });

  if (!Number.isInteger(body.quotient))
    return res.status(400).json({ msg: "El COCIENTE no es del tipo numerico" });

  if (!Number.isInteger(body.divisor))
    return res.status(400).json({ msg: "El DIVISOR no es del tipo numerico" });

  return res.status(200).json(await service.division(body));
});

module.exports = router;
